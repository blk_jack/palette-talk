---
## Whet Your App's Palette
![PALETTE](images/andyburger.jpg)
Note: October 2014?  Do you think Android Support library revision 24 just came out?
---
### What is Palette?

![whodat](images/idiot3.png)
Note:  A support library that extracts prominent colors from images. Throw a bitmap at it and get back one of 6 color swatches. 
---
![HSL](images/hslcilinder.png)
Note: These swatches are chosen from analyzing the HSL color profile of the pixels in the image and based on target ranges for luminance, saturation and population.  TLDR; Vibrant is more saturated, darker and light are obvious
---
### color vs. swatch
- i.e. getVibrantColor(DEFAULT_COLOR) vs. getVibrantSwatch()
- Swatch can be null!  Check for that.
- Swatch gives you the color (getRgb), no. of pixels (getPopulation), hue/saturation/light(getHsl)
- getBodyTextColor(), getTitleTextColor()
---
getDominantColor() (Added October 2016)

![PocketCasts](images/pocketcasts3.png)
Note: There's actually 7 color swatches, despite Google not mentioning it in their documentation.  In addition to vibrant/muted, this swatch was added in Android Support Library Revision 25 and uses pixel population to return the dominant color
---
### Targets
Custom selection of colors in a generated palette

- Vibrant/Muted/Dominant are all predefined Targets
- i.e. min/max/target lightness
- i.e. min/max/target saturation
- i.e. weight of lightness/saturation/population
Note: Targets are a custom selection of colors in a generated palette
---
### Create a Target
```kotlin
    fun getFancyPaletteTarget(): Target {
        return Target.Builder()
                .setTargetLightness(0.45f)
                .setMaximumLightness(0.8f)
                .setMinimumLightness(0.2f)
                .setMinimumSaturation(0.1f)
                .setTargetSaturation(0.5f)
                .setMaximumSaturation(0.7f)
                .setPopulationWeight(0.6f)
                .setSaturationWeight(0.4f)
                .setLightnessWeight(0.6f)
                .setExclusive(false)
                .build()
    }
```
Note: Custom target that catches more but leans towards a lighter, more dominant color with middle saturation
---?image=images/feed_compare.png&size=auto 90%
---
### Gotchas with default filters
- By default rejects colors very near black/white or red colors that are difficult for people with red-blind color blindness
- Can prevent Palette from finding a swatch for largely black & white images
---
### Tip #1
Use addTarget & clearFilters()

```kotlin
    fun fancyPalette(bitmap: Bitmap): Palette {
        return Palette.Builder(bitmap)
                .addTarget(getFancyPaletteTarget())
                .addTarget(Target.DARK_VIBRANT)
                .addTarget(Target.LIGHT_VIBRANT)
                .clearFilters()
                .generate()
    }
```
Note: Using previous target and chaining together dark and light vibrant. ClearFilters gets rid of default restrictions.
---
### Tip #2
Cache Palette results

- Helpful if using palette in a list
- Pass when instantiating a details page on list to prevent subsequent lookup
Note: Swatch generation isn't free, larger images and palette sizes take longer
---
### Tip #3
Fade in swatch color to prevent slow page load

```kotlin
    fun changeTextColor(from: Int?, to: Int?, views: TextView) {
        for (view in views) {
            changeTextColor(from, to, view)
        }
    }

    fun animateTextColor(from: Int?, to: Int?, view: TextView) {
        val colorAnim = ObjectAnimator.ofInt(view, "textColor",
                from, to)
        colorAnim.setEvaluator(ArgbEvaluator())
        colorAnim.start()
    }
```
Note: If you're using an accent color, don't wait for an image to load and then run palette before loading your page
---
<video data-video="true" class="stretch" data-autoplay="" src="http://jeff.mimic.ca/gRM.mp4" style="height: 100%; width: 100%;" googl="true" loop="true"></video>
---
### Tip #4
Manually adjust the darkness/lightness/alpha of color

```kotlin
    fun darkenColor(color: Int, darken: Float): Int {
        val hsv = FloatArray(3)
        Color.colorToHSV(color, hsv)
        hsv[2] *= darken // value component
        return Color.HSVToColor(hsv)
    }
```
Note: You can use Palette as a starting point and adjust the color accordingly
---
## and that's it!
- Use palette to make your app stand out
- Cache palette results
- Play around with a custom Target to fine tune color swatches
- Even in 2018 is Palette still worth doing a lightning talk about?
---
## GDG Toronto Android
- Jeff Corcoran (@corcoran on github)
- Android Pub Night - June 26th! - https://www.meetup.com/Android-Pub-Night/
